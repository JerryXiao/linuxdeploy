#!/bin/sh
# Linux Deploy Component
# (c) Anton Skshidlevsky <meefik@gmail.com>, GPLv3


rcservices="syslog-ng ssh"
do_start()
{
    msg -n ":: Starting ${COMPONENT} ... "
    [ -e ${CHROOT_DIR}/etc/rc.local ] && chroot_exec -u root /etc/rc.local >/dev/null 2>&1 &
    for name in $rcservices ; do
        chroot_exec -u root service ${name} start >/dev/null 2>&1
    done
    is_ok "fail" "done"
    return 0
}

do_stop()
{
    msg -n ":: Stopping ${COMPONENT} ... "
    for name in $rcservices ; do
        chroot_exec -u root service ${name} stop >/dev/null 2>&1
    done
    is_ok "fail" "done"
    return 0
}

do_status()
{
    return 0
}

do_help()
{
cat <<EOF
	Exec /etc/rc.local at boot.
EOF
}
